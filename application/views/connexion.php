<?php $this->load->view('templates/header') ?>

    <div class="container-fluid">
        <div class="col-6 mx-auto">
            
            <div class="card text-white bg-dark my-5">
                <?= form_open('connexion/connect') ?>
                    <div class="card-header text-center">
                        Connexion
                    </div>
                    
                    <div class="card-body col-8 mx-auto">
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input type="text" class="form-control" name="login" required>
                        </div>

                        <div class="form-group">
                            <label for="password">Mot de Passe</label>
                            <input type="password" class="form-control" name="password" required>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-outline-light btn-lg">Connexion</button>
                    </div>
                <?= form_close() ?>
            </div>
            

            <div class="card text-white bg-dark my-5">
                <?= form_open('connexion/subscribe') ?>
                    <div class="card-header text-center">
                        Inscription
                    </div>
                    
                    <div class="card-body col-8 mx-auto">
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input type="text" class="form-control" name="login" required>
                            <small class="form-text text-muted">Votre identifiant unique enssat</small>
                        </div>

                        <div class="form-group">
                            <label for="alias">Pseudo</label>
                            <input type="text" class="form-control" name="alias" required>
                            <small class="form-text text-muted">Vous pourrez le modifier plus tard.</small>
                        </div>

                        <div class="form-group">
                            <label for="password">Mot de Passe</label>
                            <input type="password" class="form-control"name="password" required>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <input type="hidden" name="token" value="<?= $token ?>">
                        <button type="submit" class="btn btn-outline-light btn-lg">Envoyer</button>
                    </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

<?php $this->load->view('templates/footer') ?>