<?php $this->load->view('templates/header') ?>

    <?php

        if ($ratio == 0) {
            $span = 'text-danger';

        } else if ($ratio > 0 && $ratio < 1) {
            $span = 'text-warning';

        } else {
            $span = 'text-success';
        }

    ?>

    <div class="container-fluid">

        <div class="card-deck">
            <div class="card text-white bg-dark mt-5">
                <div class="card-header text-center h4">
                    Vos statistiques
                </div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-dark">
                            Victoires ce mois-ci<span style="float: right"><?= $victoires ?></span>
                        </li>

                        <li class="list-group-item bg-dark">
                            Défaites ce mois-ci<span style="float: right"><?= $defaites ?></span>
                        </li>

                        <li class="list-group-item bg-dark">Ratio
                            <span class="<?= $span ?>" style="float: right">
                                <?= $ratio ?>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="card text-white bg-dark mt-5">
                <div class="card-header text-center h4">
                    Général
                </div>

                <div class="card-body">
                    <table class="table table-striped table-dark" id="croissantagesList">
                        <thead>
                            <tr>
                                <th>Mois</th>
                                <th>Nombre de croissantages</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php foreach ($croissantages as $key => $value): ?>
                                <tr>
                                    <td><?= $key ?></td>
                                    <td><?= $value ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="<?= assets_url('js/bars.js') ?>"></script>
<?php $this->load->view('templates/footer') ?>