<?php $this->load->view('templates/header') ?>

    <div class="container-fluid">
        <div class="card text-white bg-dark mt-5">
            <div class="card-header text-center h4">
                Derniers croissantages
                <button class="btn btn-outline-success" style="float: right;" data-toggle="modal" data-target="#addCroissantage">
                    <i class="fas fa-plus"></i> Ajouter
                </button>
            </div>

            <div class="card-body">
                <table class="table table-striped table-dark" id="croissantagesList">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Croissanteur</th>
                            <th>Croissanté</th>
                            <th>Date</th>
                            <th>Statut</th>
                            <th>Viennoiseries</th>
                            <th></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($recents as $recent): ?>
                            <tr>
                                <th class="id"><?= $recent['id'] ?></th>
                                <td class="croissanteur"><?= $recent['croissanteur'] ?></td>
                                <td class="croissante"><?= $recent['croissante'] ?></td>
                                <td class="date"><?= $this->date->convertTime($recent['date']) ?></td>
                                <td>
                                    <?php

                                        switch ($recent['state']) {
                                            case 1:
                                                echo "<span class='text-danger'>Date à fixer par la victime</span>";
                                                break;

                                            case 2:
                                                $deadline = $this->date->convertTime($recent['deadline']);
                                                echo "<span class='text-warning'>Date fixée $deadline</span>";
                                                break;

                                            case 3:
                                                $deadline = $this->date->convertTime($recent['deadline']);
                                                echo "<span class='text-success'>Honoré $deadline</span>";
                                                break;
                                        }

                                    ?>
                                </td>

                                <td>
                                    <?php
                                        $stringViennoiseries = "";

                                        foreach ($choix[$recent['id']] as $key => $value) {
                                            $stringViennoiseries .= "$key : $value / ";
                                        }
                                    ?>
                                    <a tabindex="0" class="btn btn-outline-info btn-sm text-info" role="button" data-toggle="popover" data-trigger="focus" title="Viennoiseries" data-content="<?= $stringViennoiseries ?>">Voir</a>
                                </td>

                                <td>
                                    <?php if ($recent['state'] == 2): ?>
                                        <button type='button' class='btn btn-sm btn-outline-warning' data-toggle="modal" data-target="#chooseViennoiserie">Choisir</button>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
<?php $this->load->view('templates/footer') ?>

<script>

$(function () {
  $('[data-toggle="popover"]').popover()
})

</script>