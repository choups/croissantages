
<div class="row">
    <div class="col-4 offset-lg-4">
        <div class="alert alert-danger fade show mt-4" role="alert">
            <i class="fas fa-times"></i> Ce pseudo est déjà pris !
        </div>
    </div>
</div>

<script src="<?= assets_url('js/alert-dismiss.js') ?>"></script>