
<div class="row">
    <div class="col-4 offset-lg-4">
        <div class="alert alert-danger fade show mt-4" role="alert">
            <i class="fas fa-times"></i> Un compte existe déjà avec ces identifiants !
        </div>
    </div>
</div>

<script src="<?= assets_url('js/alert-dismiss.js') ?>"></script>