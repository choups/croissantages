
<div class="row">
    <div class="col-4 offset-lg-4">
        <div class="alert alert-success fade show mt-4" role="alert">
            <i class="fas fa-check"></i> Votre viennoiserie préférée a été mise à jour !
        </div>
    </div>
</div>

<script src="<?= assets_url('js/alert-dismiss.js') ?>"></script>