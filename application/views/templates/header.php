<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Croissantages IMR 2022</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body class="bg-secondary">
        
        <?php if (isset($_SESSION['userinfo'])) $this->load->view('templates/navbar') ?>