<?php
    function setActiveIfIsCurrentPage($string) {
        $result = "";

        if (strpos(uri_string(), $string) !== false) {
            $result = 'active';
        }

        return $result;
    }
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?= base_url('croissantage') ?>">
        <img src="<?= assets_url('img/croissant.png') ?>" width="30" height="30" class="d-inline-block align-top" alt="">
        IMR 2022
    </a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link <?= setActiveIfIsCurrentPage('croissantage') ?>" href="<?= base_url('croissantage') ?>">Croissantages</a>
            <a class="nav-item nav-link <?= setActiveIfIsCurrentPage('profil')       ?>" href="<?= base_url('profil') ?>">Profil</a>
            <a class="nav-item nav-link <?= setActiveIfIsCurrentPage('etudiant')     ?>" href="<?= base_url('etudiant') ?>">Etudiants</a>
            <a class="nav-item nav-link <?= setActiveIfIsCurrentPage('statistiques') ?>" href="<?= base_url('statistiques') ?>">Statistiques</a>
            
            <?php if (isAdmin()): ?>
                <a class="nav-item nav-link <?= (uri_string() == "admin") ? 'active' : '' ?>" href="<?= base_url('admin') ?>">Panel admin</a>
            <?php endif ?>

        </div>
    </div>


    <?= form_open('connexion/disconnect') ?>
        <button type="submit" class="btn btn-outline-danger">Déconnexion</button>
    <?= form_close() ?>
</nav>