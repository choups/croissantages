<?php $this->load->view('templates/header') ?>

<div class="container-fluid">
    <div class="card-deck my-4">
        <div class="card text-white bg-dark">
            <div class="card-header text-center">
                Gestion des utilisateurs
            </div>

            <table class="table table-striped table-dark" id="userlist">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Login</th>
                        <th>Alias</th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php foreach($etudiants as $etudiant): ?>
                        <tr>
                            <th class="id"><?= $etudiant['id'] ?></th>
                            <td class="login"><?= $etudiant['login'] ?></td>
                            <td class="alias"><?= $etudiant['alias'] ?></td>
                            <td>
                                <button type="button" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#removeUser">
                                    <i class="fas fa-times"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>

        <div class="card text-white bg-dark">
            <div class="card-header text-center">
                Gestion des droits
                <button class="btn btn-sm btn-outline-success" style="float: right;" data-toggle="modal" data-target="#addDroit">
                    <i class="fas fa-plus"></i> Ajouter
                </button>
            </div>

            <div class="card-body">
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th>Login</th>
                            <th>Droit</th>
                            <th></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($etudroits as $etudroit): ?>
                            <tr>
                                <td><?= $etudroit['login'] ?></td>
                                <td><?= $etudroit['name'] ?></td>

                                <td class="row">
                                    <?= form_open('admin/removedroit') ?>
                                        <input type="hidden" name="login" value="<?= $etudroit['login'] ?>">
                                        <input type="hidden" name="name" value="<?= $etudroit['name'] ?>">
                                        
                                        <button type="button" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#removeDroit">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    <?= form_close() ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card-deck">
        <div class="card text-white bg-dark">
            <div class="card-header text-center">
                Croissantages en attente d'approbation
            </div>

            <div class="card-body">
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Croissanteur</th>
                            <th>Croissanté</th>
                            <th>Date</th>
                            <th>Décision</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($pending_cr as $pending): ?>
                            <tr>
                                <th><?= $pending['id'] ?></th>
                                <td><?= $pending['croissanteur'] ?></td>
                                <td><?= $pending['croissante'] ?></td>
                                <td><?= $this->date->convertTime($pending['date']) ?></td>

                                <td class="row">
                                    <?= form_open('admin/acceptpendingcroissantage') ?>
                                        <input type="hidden" name="id" value="<?= $pending['id'] ?>">
                                        <button type="submit" class="btn btn-sm btn-outline-primary">Accepter</button>
                                    <?= form_close() ?>
                                    
                                    <?= form_open('admin/refusependingcroissantage') ?>
                                        <input type="hidden" name="id" value="<?= $pending['id'] ?>">
                                        <button type="submit" class="btn btn-sm btn-outline-danger ml-4">Refuser</button>
                                    <?= form_close() ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card text-white bg-dark">
            <div class="card-header text-center">
                Gestion des viennoiseries
            </div>

            <div class="card-body">
                <table class="table table-striped table-dark" id="viennoiserieList">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Viennoiserie</th>
                            <th>Disponible</th>
                            <th></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($viennoiseries as $viennoiserie): ?>
                            <tr>
                                <th class="id"><?= $viennoiserie['id'] ?></th>
                                <td class="name"><?= $viennoiserie['name'] ?></td>
                                <td class="availability">
                                    <?php if ($viennoiserie['isAvailable'] == 1): ?>
                                    
                                        <span class="text-success">Oui</span>
                                    
                                    <?php else: ?>

                                        <span class="text-danger">Non</span>

                                    <?php endif ?>
                                    <input type="hidden" value="<?= $viennoiserie['isAvailable'] ?>">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-light btn-sm" data-toggle="modal" data-target="#editViennoiserie">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('templates/footer') ?>