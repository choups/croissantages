<?php $this->load->view('templates/header') ?>

    <div class="container-fluid">
        <div class="card-deck">
            <?php $this->load->view('tab/victoires'); ?>
            <?php $this->load->view('tab/defaites'); ?>
        </div>

        <?php if ($_SESSION['userinfo']['alias'] == $etudiant['alias']): ?>
            <div class="card text-white bg-dark my-4">
                <div class="card-header text-center">
                    Paramètres
                </div>

                <div class="card-body row">
                    <div class="col-3">
                        <div class="card text-white bg-dark border-secondary">
                            <?= form_open("profil/changepseudo") ?>
                                <div class="card-header text-center">
                                    Modifier votre pseudo
                                </div>

                                <div class="card-body">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="alias" value="<?= $_SESSION['userinfo']['alias'] ?>" required>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-outline-success">Envoyer</button>
                                </div>
                            <?= form_close() ?>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="card text-white bg-dark border-secondary">
                            <?= form_open("profil/changepassword") ?>
                                <div class="card-header text-center">
                                    Modifier votre mot de passe
                                </div>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="old_password" placeholder="Ancien mot de passe" required>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="new_password" placeholder="Nouveau mot de passe" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-outline-success">Envoyer</button>
                                </div>
                            <?= form_close() ?>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="card text-white bg-dark border-secondary">
                            <?= form_open("profil/changeviennoiserie") ?>
                                <div class="card-header text-center">
                                    Modifier votre viennoiserie préférée
                                </div>

                                <div class="card-body">
                                    <div class="form-group">
                                        <select class="form-control" name="viennoiserie" required>
                                            <?php foreach ($viennoiseries as $viennoiserie): ?>

                                                <?php if ($viennoiserie['isAvailable'] == 1): ?>
                                            
                                                    <option value="<?= $viennoiserie['id'] ?>"
                                                            <?= ($viennoiserie['id'] == $_SESSION['userinfo']['id_v']) ? 'selected' : '' ?>>
                                                        <?= $viennoiserie['name'] ?>
                                                    </option>
                                                
                                                <?php endif;?>
                                                
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-outline-success">Envoyer</button>
                                </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php

                if (isset($_GET['param'])) {
                    
                    switch ($_GET['param']) {

                        case 'viennoiserie_ok':
                            $this->load->view('alerts/viennoiserie_ok');
                            break;

                        case 'alias_ok':
                            $this->load->view('alerts/alias_ok');
                            break;

                        case 'alias_error':
                            $this->load->view('alerts/alias_error');
                            break;

                        case 'password_ok':
                            $this->load->view('alerts/password_ok');
                            break;

                        case 'password_error':
                            $this->load->view('alerts/password_error');
                            break;
                    }
                }
            ?>

        <?php endif; ?>
    </div>

<?php $this->load->view('templates/footer') ?>