<?php $this->load->view('templates/header') ?>

    <div class="container-fluid">
        <div class="card text-white bg-dark mt-5">
            <div class="card-header text-center h4">
                Détails du croissantage par <span class="text-danger"><?= $croissanteur['alias'] ?></span>
            </div>

            <div class="card-body">
                <div class="alert alert-primary" role="alert">
                    Date: <?= $croissantage['date'] ?>
                </div>

                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th>Pseudo</th>
                            <th>Viennoiserie</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($commandes as $commande): ?>
                            <tr>
                                <td><?= $commande['alias'] ?></td>
                                <td><?= $commande['viennoiserie'] ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
<?php $this->load->view('templates/footer') ?>