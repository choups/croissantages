
<div class="modal fade" id="addCroissantage" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nouveau croissantage</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <?= form_open('croissantage/create') ?>
                <div class="modal-body">
                    <div class="row">
                        <div class="col form-group">
                            <label for="croissanteur">Croissanteur</label>
                            <select class="form-control" name="croissanteur" id="croissanteur" required>
                                <?php foreach ($etudiants as $etudiant): ?>

                                    <option value="<?= $etudiant['id'] ?>"
                                            <?= ($etudiant['id'] == $_SESSION['userinfo']['id']) ? 'selected' : '' ?>>
                                        <?= $etudiant['alias'] ?>
                                    </option>
                                    
                                <?php endforeach ?>
                            </select>
                        </div>

                        <div class="col form-group">
                            <label for="croissante">Croissanté</label>
                            <select class="form-control" name="croissante" id="croissante" required>
                                <?php foreach ($etudiants as $etudiant): ?>
                                
                                    <option value="<?= $etudiant['id'] ?>">
                                        <?= $etudiant['alias'] ?>
                                    </option>
                                    
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col form-group">
                            <label for="date">Date</label>
                            <input class="form-control" type="date" placeholder="jj/mm/aaaa" name="date" id="date" value="<?= $currentDay ?>">
                        </div>

                        <div class="col form-group">
                            <label for="time">Heure</label>
                            <input class="form-control" type="time" placeholder="HH:mm" name="time" id="time" value="<?= $currentTime ?>">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Soumettre</button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script src="<?= assets_url('js/add-croissantage-modal.js') ?>"></script>