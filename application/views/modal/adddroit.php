
<div class="modal fade" id="addDroit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'un droit</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <?= form_open('admin/ajouterdroit') ?>
                <div class="modal-body">
                    <div class="row">
                        <div class="col form-group">
                            <label for="login">Utilisateur</label>
                            <select class="form-control" name="id_etu" id="login" required>
                                <?php foreach ($etudiants as $etudiant): ?>

                                    <option value="<?= $etudiant['id'] ?>">
                                        <?= $etudiant['login'] ?>
                                    </option>
                                    
                                <?php endforeach ?>
                            </select>
                        </div>

                        <div class="col form-group">
                            <label for="droit">Droit</label>
                            <select class="form-control" name="id_droit" id="droit" required>
                                <?php foreach ($droits as $droit): ?>
                                
                                    <option value="<?= $droit['id'] ?>">
                                        <?= $droit['name'] ?>
                                    </option>

                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
