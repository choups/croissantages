
<div class="modal fade" id="editViennoiserie" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editer viennoiserie</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <?= form_open('admin/editviennoiserie') ?>
                <div class="modal-body row">
                    <input type="hidden" name="id_v" id="id_v">

                    <div class="input-group mx-3">
                        <input type="text" class="form-control" placeholder="Nom de la viennoiserie" name="name_v" id="name_v">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" name="checkbox" id="checkbox">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script src="<?= assets_url('js/editviennoiserie.js') ?>"></script>