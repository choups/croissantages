
<div class="modal fade" id="fixerDate" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Fixer date</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <?= form_open('croissantage/fixerdate') ?>
                <div class="modal-body row">
                    <div class="col form-group">
                        <label for="date">Date</label>
                        <input class="form-control" type="date" placeholder="jj/mm/aaaa" name="date" value="<?= $currentDay ?>" required>
                    </div>

                    <div class="col form-group">
                        <label for="date">Heure</label>
                        <input class="form-control" type="time" placeholder="hh:ss" name="time" value="<?= $currentTime ?>" required>
                    </div>

                    <input type="hidden" name="id_cr" value="" id="id_cr" required>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Fixer</button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script src="<?= assets_url('js/fixerdate-modal.js') ?>"></script>