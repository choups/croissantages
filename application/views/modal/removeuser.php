
<div class="modal fade" id="removeUser" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Suppression d'un utilisateur</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <?= form_open('admin/removeuser') ?>
                <div class="modal-body">
                    <p>Êtes-vous sûr(e) de vouloir supprimer <span id="userlogin"></span> ?</p>
                    <input type="hidden" name="userid" value="" required>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script src="<?= assets_url('js/user-delete-modal.js') ?>"></script>