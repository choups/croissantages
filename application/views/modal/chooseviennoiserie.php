
<div class="modal fade" id="chooseViennoiserie" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Choisir viennoiserie</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <?= form_open('croissantage/chooseviennoiserie') ?>
                <div class="modal-body">
                    
                    <div class="form-group">
                        <input type="hidden" name="id_cr" value="">
                        
                        <small id="catchphrase" class="form-text text-muted"></small>
                        <select class="form-control" name="viennoiserie" required>
                            <?php foreach ($viennoiseries as $viennoiserie): ?>

                                <?php if ($viennoiserie['isAvailable'] == 1): ?>

                                    <option value="<?= $viennoiserie['id'] ?>"
                                        <?= ($viennoiserie['id'] == $_SESSION['userinfo']['id_v']) ? 'selected' : '' ?>>
                                        <?= $viennoiserie['name'] ?>
                                    </option>

                                <?php endif;?>

                            <?php endforeach ?>
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Envoyer</button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script src="<?= assets_url('js/chooseviennoiserie.js') ?>"></script>