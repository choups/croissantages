<?php $this->load->view('templates/header') ?>

    <div class="container-fluid">
        <div class="card text-white bg-dark mt-5">
            <div class="card-header text-center h4">
                Derniers croissantages
                <button class="btn btn-outline-success" style="float: right;" data-toggle="modal" data-target="#addCroissantage">
                    <i class="fas fa-plus"></i> Ajouter
                </button>
            </div>

            <div class="card-body">
                <table class="table table-striped table-dark" id="croissantagesList">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Login</th>
                            <th>Pseudo</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($etudiants as $etudiant): ?>
                            <?= form_open('profil/voirdetails') ?>
                                <tr>
                                    <th><?= $etudiant['id'] ?></th>
                                    <td><?= $etudiant['login'] ?></td>
                                    <td><?= $etudiant['alias'] ?></td>
                                    <td>
                                        <input type='hidden' value='<?= $etudiant['id'] ?>' name="id_etu">
                                        <button type='submit' class='btn btn-sm btn-outline-info'>Voir</button>
                                    </td>
                                </tr>
                            <?= form_close() ?>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
<?php $this->load->view('templates/footer') ?>