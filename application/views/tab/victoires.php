<div class="card text-white bg-dark mt-4">
    <div class="card-header text-center">
        <?php 
            if ($_SESSION['userinfo']['alias'] == $etudiant['alias']) {
                echo "Vos victoires";

            } else {
                echo "Victoires de " . $etudiant['alias'];
            }
        ?>
    </div>

    <div class="card-body">
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nom</th>
                    <th>Date</th>
                    <th>Statut</th>
                    <th></th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($victoires as $victoire): ?>
                    <tr>
                        <th><?= $victoire['id'] ?></th>
                        <td><?= $victoire['alias'] ?></td>
                        <td><?= $this->date->convertTime($victoire['date']) ?></td>
                        <?php if ($victoire['state'] == 1): ?>

                            <td class='text-danger'>Date à fixer</td>
                            
                        <?php elseif ($victoire['state'] == 2): ?>

                            <td class='text-warning'>Date fixée</td>
                            <td class="row">
                                <?php if (isAdmin() || $_SESSION['userinfo']['alias'] == $etudiant['alias']): ?>
                                    <?= form_open('croissantage/marquerhonore') ?>
                                        <input type='hidden' name='id_cr' value='<?= $defaite['id'] ?>'>
                                        <button class='btn btn-outline-warning btn-sm mr-1'>
                                            Marquer comme honoré
                                        </button>
                                    <?= form_close() ?>
                                <?php endif; ?>

                                <button type='button' class='btn btn-sm btn-outline-warning' data-toggle="modal" data-target="#chooseViennoiserie">Choisir</button>
                            </td>

                        <?php elseif ($victoire['state'] == 3): ?>

                            <td class='text-success'>Honoré</td>

                        <?php endif ?>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>