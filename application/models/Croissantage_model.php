<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Croissantage_model extends CI_Model {

    function create($idCer, $idCed, $date, $state = 0, $deadline = NULL) {
        $query_str = "INSERT INTO croissantage (idCer, idCed, date, state, deadline)
                        VALUES (:idCer, :idCed, :date, :state, :deadline)";

        $input_data = array(
            "idCer"         =>  strip_tags($idCer),
            "idCed"         =>  strip_tags($idCed),
            "date"          =>  strip_tags($date),
            "state"         =>  strip_tags($state),
            "deadline"      =>  strip_tags($deadline)
       );

        $query = $this->dbh->prepare($query_str);

        $query->execute($input_data);
    }


    function readAll() {
        $query_str = "SELECT e1.alias as croissanteur, e2.alias as croissante, c.id, c.date, c.state
                        FROM croissantage as c
                        
                        INNER JOIN etudiant as e1
                        ON c.idCer = e1.id
                        
                        INNER JOIN etudiant as e2
                        ON c.idCed = e2.id";

        $input_data = array(
            "date"  =>  date("Y-m-d H:i:s", strtotime('-1 month'))
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readAllPublic($numberOfMonthsAgo) {
        $query_str = "SELECT e1.alias as croissanteur, e2.alias as croissante, c.id, c.date, c.state, c.deadline
                        FROM croissantage as c
                        
                        INNER JOIN etudiant as e1
                        ON c.idCer = e1.id
                        
                        INNER JOIN etudiant as e2
                        ON c.idCed = e2.id
                        
                        WHERE c.date >= :date
                        AND c.state != 0";

        $input_data = array(
            "date"  =>  date("Y-m-d H:i:s", strtotime("-$numberOfMonthsAgo month"))
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readAllPending() {
        $query_str = "SELECT e1.alias as croissanteur, e2.alias as croissante, c.id, c.date, c.state
                        FROM croissantage as c
                        
                        INNER JOIN etudiant as e1
                        ON c.idCer = e1.id
                        
                        INNER JOIN etudiant as e2
                        ON c.idCed = e2.id
                        
                        WHERE c.state = 0";

        $input_data = array(
            "date"  =>  date("Y-m-d H:i:s", strtotime('-1 month'))
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function read($id) {
        $query_str = "SELECT *
                        FROM croissantage
                        WHERE id = :id";

        $input_data = array("id" => strip_tags($id));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        $data = $query->fetch();

        return $data;
    }

    function readFromIdCer($idEtu) { //Winner
        $query_str = "SELECT c.*, e.alias
                        FROM croissantage as c
                        
                        INNER JOIN etudiant as e
                        ON c.idCed = e.id
                        
                        WHERE c.idCer = :idCer";

        $input_data = array("idCer" => strip_tags($idEtu));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readFromIdCed($idEtu) { //Loser
        $query_str = "SELECT c.*, e.alias
                        FROM croissantage as c

                        INNER JOIN etudiant as e
                        ON c.idCer = e.id
                        
                        WHERE c.idCed = :idCed";

        $input_data = array("idCed" => strip_tags($idEtu));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function update($id, $idCer, $idCed, $date) {
        $query_str = "UPDATE croissantage
                        SET     idCer   = :idCer,
                                idCed   = :idCed
                                date   = :date
                        WHERE   id      = :id";

        $input_data = array(
            "idCer" =>  strip_tags($idCer),
            "idCed" =>  strip_tags($idCed),
            "date"  =>  strip_tags($date),
            "id"    =>  strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function updateState($id, $state = 0) {
        $query_str = "UPDATE croissantage
                        SET state = :state
                        WHERE id = :id";

        $input_data = array(
            "state" =>  strip_tags($state),
            "id"    =>  strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function updateDeadline($id, $deadline) {
        $query_str = "UPDATE croissantage
                        SET deadline = :deadline
                        WHERE id = :id";

        $input_data = array(
            "deadline"  => strip_tags($deadline),
            "id"        => strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function delete($id) {
        $query_str = "DELETE FROM croissantage
                        WHERE id = :id";

        $input_data = array("id" => strip_tags($id));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function deleteAllFromUser($userid) {
        $query_str = "UPDATE croissantage
                        SET idCer = 0
                        WHERE idCer = :id";

        $query1 = $this->dbh->prepare($query_str);

        
        $query_str = "UPDATE croissantage
                        SET idCed = 0
                        WHERE idCed = :id";

        $query2 = $this->dbh->prepare($query_str);
        

        $input_data = array("id" => strip_tags($userid));

        $query1->execute($input_data);
        $query2->execute($input_data);
    }
}