<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Etudroit_model extends CI_Model {
    
    function create($id_etu, $id_droit) {
        $query_str = "INSERT INTO etudiant_droit (id_etu, id_droit)
                        VALUES (:id_etu, :id_droit)";

        $input_data = array(
            "id_etu"    =>  strip_tags($id_etu),
            "id_droit"  =>  strip_tags($id_droit)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function readAll() {
        $query_str = "SELECT e.login, d.name
                        FROM etudiant_droit as ed
                        
                        INNER JOIN etudiant as e
                        ON e.id = ed.id_etu
                        
                        INNER JOIN droit as d
                        ON d.id = ed.id_droit";

        $query = $this->dbh->query($query_str);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function read($id_etu, $id_droit) {
        $query_str = "SELECT * FROM etudiant_droit

                        WHERE id_etu = :id_etu
                        AND id_droit = :id_droit";

        $input_data = array(
            "id_etu"    =>  strip_tags($id_etu),
            "id_droit"  =>  strip_tags($id_droit)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readFromIdEtu($id_etu) {
        $query_str = "SELECT d.name, d.power
                        FROM etudiant_droit as ed
                        
                        INNER JOIN droit as d
                        on d.id = ed.id_droit

                        WHERE id_etu = :id_etu";

        $input_data = array(
            "id_etu"    =>  strip_tags($id_etu)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readFromIdDroit($id_droit) {
        $query_str = "SELECT d.name, d.power
                        FROM etudiant_droit as ed
                        
                        INNER JOIN droit as d
                        on d.id = ed.id_droit

                        WHERE id_droit = :id_droit";

        $input_data = array(
            "id_droit"  =>  strip_tags($id_droit)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        return $data;
    }


    function delete($id_etu, $id_droit) {
        $query_str = "DELETE FROM etudiant_droit
                        WHERE id_etu = :id_etu
                        AND   id_droit = :id_droit";

        $input_data = array(
            "id_etu"    =>  strip_tags($id_etu),
            "id_droit"  =>  strip_tags($id_droit)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);  
    }


    function deleteEtu($id_etu) {
        $query_str = "DELETE FROM etudiant_droit
                        WHERE id_etu = :id_etu";

        $input_data = array(
            "id_etu"    =>  strip_tags($id_etu)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);  
    }


    function deleteDroit($id_droit) {
        $query_str = "DELETE FROM etudiant_droit
                        WHERE id_droit = :id_droit";

        $input_data = array(
            "id_droit"  =>  strip_tags($id_droit)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);  
    }
}