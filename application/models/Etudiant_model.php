<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Etudiant_model extends CI_Model {

    function create($login, $alias, $password, $id_v) {
        $query_str = "INSERT INTO etudiant(login, alias, password, id_v)
                        VALUES (:login, :alias, :password, :id_v)";

        $input_data = array(
            "login"     =>  strip_tags($login),
            "alias"     =>  strip_tags($alias),
            "password"  =>  strip_tags($password),
            "id_v"      =>  strip_tags($id_v)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $this->load->model('etudroit_model');
        $id_etu = $this->dbh->lastInsertId();
        $this->etudroit_model->create($id_etu, 3);
    }
    

    function readAll() {
        $query_str = "SELECT id, login, alias, id_v
                        FROM etudiant
                        WHERE id != 0";

        $query = $this->dbh->query($query_str);
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function read($id) {
        $query_str = "SELECT login, alias FROM etudiant
                        WHERE id = :id";

        $input_data = array("id" => strip_tags($id));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        
        $data = $query->fetch();

        return $data;
    }


    function readFromLogin($login) {
        $query_str = "SELECT * FROM etudiant
                        WHERE login = :login";

        $input_data = array("login" => strip_tags($login));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetch(PDO::FETCH_ASSOC);

        return $data;
    }


    function readFromAlias($alias) {
        $query_str = "SELECT * FROM etudiant
                        WHERE alias = :alias";

        $input_data = array("alias" => strip_tags($alias));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetch(PDO::FETCH_ASSOC);

        return $data;
    }


    function updateAlias($id, $alias) {
        $query_str = "UPDATE etudiant
                        SET alias = :alias
                        WHERE id = :id";

        $input_data = array(
            "id"    =>  strip_tags($id),
            "alias" =>  strip_tags($alias)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function updateViennoiserie($id, $id_v) {
        $query_str = "UPDATE etudiant
                        SET id_v = :id_v
                        WHERE id = :id";

        $input_data = array(
            "id_v"  =>  strip_tags($id_v),
            "id"    =>  strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function delete($id) {
        $this->load->model('etudroit_model');
        $this->etudroit_model->deleteEtu($id);

        $this->load->model('croissantage_model');
        $this->croissantage_model->deleteAllFromUser($id);

        $query_str = "DELETE FROM etudiant
                        WHERE id = :id";

        $input_data = array("id" => strip_tags($id));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }
    


    /***************** PASSWORD *******************/

    function changePassword($id, $password) {
        $query_str = "UPDATE etudiant
                        SET password = :password
                        WHERE id = :id";

        $input_data = array(
            "id"        =>  strip_tags($id),
            "password"  =>  strip_tags($password)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }
}