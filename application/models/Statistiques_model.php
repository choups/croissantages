<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Statistiques_model extends CI_Model {

    function readFromIdCer($idEtu) {
        $query_str = "SELECT count(id) as victoires
                        FROM croissantage
                        WHERE idCer = :idCer";

        $input_data = array("idCer" => strip_tags($idEtu));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        $data = $query->fetch(PDO::FETCH_NUM);

        return $data[0];
    }


    function readFromIdCed($idEtu) {
        $query_str = "SELECT count(id) as defaites
                        FROM croissantage
                        WHERE idCed = :idCed";

        $input_data = array("idCed" => strip_tags($idEtu));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        $data = $query->fetch(PDO::FETCH_NUM);

        return $data[0];
    }


    function getCroissantagesFromMonth($month) {
        $query_str = "SELECT COUNT(*) as nb
                        FROM croissantage as c

                        WHERE c.date >= :dateDeb
                        AND c.date < :dateFin
                        AND c.state != 0";

        $input_data = array(
            "dateDeb"  =>  date("Y-m-d H:i:s", strtotime("$month")),
            "dateFin"  =>  date("Y-m-d H:i:s", strtotime("$month + 1 month"))
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetch(PDO::FETCH_NUM);

        return $data[0];
    }
}