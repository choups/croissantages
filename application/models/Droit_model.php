<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Droit_model extends CI_Model {
    
    function create($name, $power = NULL) {
        $query_str = "INSERT INTO droit (name, power)
                            VALUES (:name, :power)";

        $input_data = array(
            "name"  =>  strip_tags($name),
            "power" =>  strip_tags($power)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function readAll() {
        $query_str = "SELECT * FROM droit";

        $query = $this->dbh->query($query_str);
        
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function read($id) {
        $query_str = "SELECT * FROM droit
                        WHERE id = :id";

        $input_data = array(
            "id"    =>  strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readFromName($name) {
        $query_str = "SELECT * FROM droit
                        WHERE name = :name";

        $input_data = array(
            "name"    =>  strip_tags($name)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function update($id, $name, $power) {
        $query_str = "UPDATE droit
                        SET name  = :name,
                            power = :power
                        WHERE id = :id";

        $input_data = array(
            "name"  =>  strip_tags($name),
            "power" =>  strip_tags($power),
            "id"    =>  strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);    
    }


    function delete($id) {
        $this->load->model('etudroit_model');
        $this->etudroit_model->deleteDroit($id);

        $query_str = "DELETE FROM droit
                        WHERE id = :id";

        $input_data = array(
            "id" => strip_tags($id)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);  
    }
}