<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Croissantage_viennoiserie_model extends CI_Model {

    function create($id_cr, $id_etu, $id_v) {
        $query_str = "INSERT INTO croissantage_viennoiserie (id_cr, id_etu, id_v)
                        VALUES (:id_cr, :id_etu, :id_v)";

        $input_data = array(
            "id_cr"     =>  strip_tags($id_cr),
            "id_etu"    =>  strip_tags($id_etu),
            "id_v"      =>  strip_tags($id_v)
       );

        $query = $this->dbh->prepare($query_str);

        $query->execute($input_data);
    }

    
    function read($id_cr, $id_etu) {
        $query_str = "SELECT *
                        FROM croissantage_viennoiserie
                        WHERE id_cr = :id_cr
                        AND id_etu = :id_etu";

        $input_data = array(
            "id_cr"     =>  strip_tags($id_cr),
            "id_etu"    =>  strip_tags($id_etu)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function readFromIdCr($id_cr) {
        $query_str = "SELECT c.idCer, e.alias, v.name as viennoiserie
                        FROM croissantage_viennoiserie as cv

                        INNER JOIN croissantage as c
                        ON c.id = cv.id_cr

                        INNER JOIN etudiant as e
                        ON e.id = cv.id_etu

                        INNER JOIN viennoiserie as v
                        ON v.id = cv.id_v
                        
                        WHERE id_cr = :id_cr";

        $input_data = array("id_cr" => strip_tags($id_cr));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function countFromIdCr_forIdV($id_cr, $id_v) {
        $query_str = "SELECT count(id_v) as nb
                        FROM croissantage_viennoiserie
                                                
                        WHERE id_cr = :id_cr
                        AND id_v = :id_v";

        $input_data = array(
            "id_cr" => strip_tags($id_cr),
            "id_v"  => strip_tags($id_v)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);

        $data = $query->fetch(PDO::FETCH_NUM);

        return $data[0];
    }

    
    function updateViennoiserie($id_cr, $id_etu, $id_v) {
        $query_str = "UPDATE croissantage_viennoiserie
                        SET     id_v    = :id_v
                        WHERE   id_cr   = :id_cr
                        AND     id_etu  = :id_etu";

        $input_data = array(
            "id_etu"    =>  strip_tags($id_etu),
            "id_v"      =>  strip_tags($id_v),
            "id_cr"     =>  strip_tags($id_cr)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }
}