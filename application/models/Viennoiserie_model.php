<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Viennoiserie_model extends CI_Model {

    function create($name, $isAvailable = 1) {
        $query_str = "INSERT INTO viennoiserie (name, isAvailable)
                        VALUES (:name, :isAvailable)";

        $input_data = array(
            "name"          =>  strip_tags($name),
            "isAvailable"   =>  strip_tags($isAvailable)
        );

        $query = $this->dbh->prepare($query_str);

        $query->execute($input_data);
    }


    function readAll() {
        $query_str = "SELECT * FROM viennoiserie";

        $query = $this->dbh->query($query_str);
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function read($id) {
        $query_str = "SELECT *
                        FROM viennoiserie
                        WHERE id = :id";

        $input_data = array("id" => strip_tags($id));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }


    function updateName($id, $name) {
        $query_str = "UPDATE viennoiserie
                        SET name = :name
                        WHERE id = :id";

        $input_data = array(
            "id"    =>  strip_tags($id),
            "name"  =>  strip_tags($name)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function updateAvailability($id, $isAvailable) {
        $query_str = "UPDATE viennoiserie
                        SET isAvailable = :isAvailable
                        WHERE id = :id";

        $input_data = array(
            "id"            =>  strip_tags($id),
            "isAvailable"   =>  strip_tags($isAvailable)
        );

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }


    function delete($id) {
        $query_str = "DELETE FROM viennoiserie
                        WHERE id = :id";

        $input_data = array("id" => strip_tags($id));

        $query = $this->dbh->prepare($query_str);
        $query->execute($input_data);
    }
}