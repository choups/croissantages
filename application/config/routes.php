<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['connexion']             =   'connexion';
$route['croissantage']          =   'croissantage';
$route['admin']                 =   'admin';
$route['profil']                =   'profil';
$route['statistiques']          =   'statistiques';


$route['default_controller'] = 'connexion';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
