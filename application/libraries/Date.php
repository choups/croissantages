<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Date {

    public function convertTime($datetime = NULL) {
        $result = "";

        if ($datetime != NULL) {

            $tmp = explode(' ', $datetime);
        
            $date = explode('-', $tmp[0]);
            $time = explode(':', $tmp[1]);
        
            $result_date = $date[2] . '/' . $date[1] . '/' . $date[0];
            $result_time = $time[0] . ':' . $time[1];
        
            $result = 'le ' . $result_date . ' à ' . $result_time;
        }

        return $result;
    }


    public function getCurrentDate() {
        return date('Y-m-d', time());
    }


    public function getCurrentTime() {
        return date('H:i', time());
    }
}
