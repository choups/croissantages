<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends CI_Controller {
    
    public function index() {
        $this->load->model('etudiant_model');
        $this->load->model('croissantage_model');
        $this->load->model('etudroit_model');
        $this->load->model('droit_model');
        $this->load->model('viennoiserie_model');

        if (isAdmin()) {
            $data = array(
                "etudiants"     =>  $this->etudiant_model->readAll(),
                "droits"        =>  $this->droit_model->readAll(),
                "etudroits"     =>  $this->etudroit_model->readAll(),
                "croissantages" =>  $this->croissantage_model->readAll(),
                "pending_cr"    =>  $this->croissantage_model->readAllPending(),
                "viennoiseries" =>  $this->viennoiserie_model->readAll()
            );

            $this->load->view('adminpanel', $data);
            $this->load->view('modal/adddroit');
            $this->load->view('modal/removeuser');
            $this->load->view('modal/editviennoiserie');

        } else {
            redirect('connexion');
        }
    }


    public function removeUser() {

        if (isAdmin() && isset($_POST['userid'])) {
            $this->load->model('etudiant_model');

            $this->etudiant_model->delete($_POST['userid']);

            redirect('admin');
        
        } else {
            redirect('connexion');
        }
    }


    public function ajouterDroit() {
        
        if (isAdmin() && isset($_POST['id_etu']) && isset($_POST['id_droit'])) {
            $this->load->model('etudroit_model');

            $data = $this->etudroit_model->read($_POST['id_etu'], $_POST['id_droit']);

            if (empty($data)) {
                $this->etudroit_model->create($_POST['id_etu'], $_POST['id_droit']);
            }
        }

        redirect('admin');
    }


    public function acceptPendingCroissantage() {
        
        if (isAdmin() && isset($_POST['id'])) {
            $this->load->model('croissantage_model');

            $this->croissantage_model->updateState($_POST['id'], 1);
        }

        redirect('admin');
    }


    public function refusePendingCroissantage() {

        if (isAdmin() && isset($_POST['id'])) {
            $this->load->model('croissantage_model');
            
            $this->croissantage_model->delete($_POST['id']);
        }

        redirect('admin');
    }


    public function editViennoiserie() {

        if (isAdmin() && isset($_POST['id_v']) && isset($_POST['name_v'])) {
            $this->load->model('viennoiserie_model');

            $availability = (isset($_POST['checkbox']) && $_POST['checkbox'] == 'on') ? 1 : 0;

            $this->viennoiserie_model->updateName($_POST['id_v'], $_POST['name_v']);
            $this->viennoiserie_model->updateAvailability($_POST['id_v'], $availability);
        }

        redirect('admin');
    }
}
