<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Connexion extends CI_Controller {

	public function index() {

        if (isConnected()) {
            redirect('croissantage');

        } else {
            if (!isset($_SESSION['token'])) {
                $_SESSION['token'] = bin2hex(random_bytes(32));
            }

            $this->load->model('viennoiserie_model');

            $data = array(
                "token"         =>  $_SESSION['token'],
                "viennoiseries" =>  $this->viennoiserie_model->readAll()
            );
            
            $this->load->view('connexion', $data);
        }
    }
    

    public function connect() {
        $data = array();
        
        if (isset($_POST['login']) && isset($_POST['password'])) {
            $this->load->model('etudiant_model');

            $data = $this->etudiant_model->readFromLogin($_POST['login']);
        }

        if ( ! empty($data) && password_verify($_POST['password'], $data['password'])) {
            $_SESSION['userinfo'] = $data;

            redirect('droit');
        }
        
        redirect('connexion');
    }


    public function disconnect() {
        session_destroy();
        redirect('connexion');
    }


    public function subscribe() {

        if (isset($_SESSION['token']) && isset($_POST['token']) && !empty($_SESSION['token']) && !empty($_POST['token'])) {
            
            if ($_SESSION['token'] == $_POST['token']) {
                
                if (isset($_POST['login']) && isset($_POST['alias']) && isset($_POST['password'])) {
                    $this->load->model('etudiant_model');

                    $dataFromLogin = $this->etudiant_model->readFromLogin($_POST['login']);
                    $dataFromAlias = $this->etudiant_model->readFromAlias($_POST['alias']);

                    if (empty($dataFromLogin) && empty($dataFromAlias)) {
                        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                    
                        $this->etudiant_model->create($_POST['login'], $_POST['alias'], $password, 1);
                        // 1 => croissant par défaut, l'utilisateur pourra changer dans ses paramètres plus tard
                    
                    } else {
                        redirect('connexion?param=already_registered');
                    }
                }
            }
        }

        redirect('connexion');
    }
}
