<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Droit extends CI_Controller {

    public function index() {

        if (isConnected()) {
            $this->load->model('etudroit_model');
            $this->load->model('droit_model');
            
            $droits = $this->etudroit_model->readFromIdEtu($_SESSION['userinfo']['id']);

            $_SESSION['userinfo']['droits'] = array();

            foreach ($droits as $droit) {
                array_push($_SESSION['userinfo']['droits'], $droit['name']);
            }
        }

        redirect('connexion');
    }
}