<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Etudiant extends CI_Controller {
    
    public function index() {

        if (isConnected()) {
            $this->load->model('etudiant_model');

            $data = array(
                "etudiants" =>  $this->etudiant_model->readAll()
            );

            $this->load->view('etudiants', $data);
        
        } else {
            redirect('connexion');
        }
    }
}