<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

    public function index($params = NULL) {

        if (isConnected()) {
            $this->load->model('etudiant_model');
            $this->load->model('croissantage_model');
            $this->load->model('viennoiserie_model');

            $data = array(
                "etudiant"      =>  $this->etudiant_model->read($_SESSION['userinfo']['id']),
                "victoires"     =>  $this->croissantage_model->readFromIdCer($_SESSION['userinfo']['id']),
                "defaites"      =>  $this->croissantage_model->readFromIdCed($_SESSION['userinfo']['id']),
                "viennoiseries" =>  $this->viennoiserie_model->readAll(),
                "currentDay"    =>  $this->date->getCurrentDate(),
                "currentTime"   =>  $this->date->getCurrentTime()
            );

            if ($params != NULL) $data['params'] = $params;

            $this->load->view('profil', $data);
            $this->load->view('modal/fixerdate');
        
        } else {
            redirect('connexion');
        }
    }


    public function changeViennoiserie() {

        if (isset($_SESSION['userinfo'])) {
            $this->load->model('etudiant_model');

            if (isset($_POST['viennoiserie'])) {
                $_SESSION['userinfo']['id_v'] = $_POST['viennoiserie'];
                $data = $this->etudiant_model->updateViennoiserie($_SESSION['userinfo']['id'], $_POST['viennoiserie']);

                redirect('profil?param=viennoiserie_ok');
            }
        }

        redirect('profil');
    }


    public function changePseudo() {

        if (isset($_SESSION['userinfo']) && isset($_POST['alias'])) {
            $this->load->model('etudiant_model');

            if (empty($this->etudiant_model->readFromAlias($_POST['alias']))) {
                
                $_SESSION['userinfo']['alias'] = $_POST['alias'];
                $data = $this->etudiant_model->updateAlias($_SESSION['userinfo']['id'], $_POST['alias']);
                
                redirect('profil?param=alias_ok');
            
            } else {
                redirect('profil?param=alias_error');
            }
        }

        redirect('profil');
    }


    public function changePassword() {

        if (isset($_SESSION['userinfo']) && isset($_POST['old_password']) && isset($_POST['new_password'])) {
            $this->load->model('etudiant_model');

            if (password_verify($_POST['old_password'], $_SESSION['userinfo']['password'])) {

                $_SESSION['userinfo']['password'] = password_hash($_POST['new_password'], PASSWORD_DEFAULT);

                $this->etudiant_model->changePassword($_SESSION['userinfo']['id'], $_SESSION['userinfo']['password']);
                
                redirect('profil?param=password_ok');
            
            } else {
                redirect('profil?param=password_error');
            }
        }

        redirect('profil');
    }


    public function voirDetails() {
        
        if (isConnected() && isset($_POST['id_etu'])) {
            $this->load->model('croissantage_model');
            $this->load->model('etudiant_model');
            $this->load->model('viennoiserie_model');

            $data = array(
                "etudiant"      =>  $this->etudiant_model->read($_POST['id_etu']),
                "victoires"     =>  $this->croissantage_model->readFromIdCer($_POST['id_etu']),
                "defaites"      =>  $this->croissantage_model->readFromIdCed($_POST['id_etu']),
                "viennoiseries" =>  $this->viennoiserie_model->readAll(),
                "currentDay"    =>  $this->date->getCurrentDate(),
                "currentTime"   =>  $this->date->getCurrentTime()
            );

            $this->load->view('profil', $data);
            $this->load->view('modal/fixerdate');
            $this->load->view('modal/chooseviennoiserie');
        
        } else {
            redirect('etudiant');
        }
    }
}