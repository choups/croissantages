<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Croissantage extends CI_Controller {
    
	public function index() {
        
        if (isConnected()) {
            $this->load->model('etudiant_model');
            $this->load->model('croissantage_model');
            $this->load->model('viennoiserie_model');
            $this->load->model('croissantage_viennoiserie_model');
            
            $data = array(
                "recents"       =>  $this->croissantage_model->readAllPublic(1),
                "etudiants"     =>  $this->etudiant_model->readAll(),
                "currentDay"    =>  $this->date->getCurrentDate(),
                "currentTime"   =>  $this->date->getCurrentTime(),
                "viennoiseries" =>  $this->viennoiserie_model->readAll(),
                "choix"         =>  NULL
            );

            foreach ($data['recents'] as $croissantage) {
                
                foreach($data['viennoiseries'] as $viennoiserie) {
                    $n = $this->croissantage_viennoiserie_model->countFromIdCr_forIdV($croissantage['id'], $viennoiserie['id']);
                    $data['choix'][$croissantage['id']][$viennoiserie['name']] = $n;
                }
            }

            $this->load->view('croissantage', $data);
            $this->load->view('modal/addcroissantage');
            $this->load->view('modal/chooseviennoiserie');
        
        } else {
            redirect('connexion');
        }
    }


    public function create() {
        
        if (isConnected() && isset($_POST['croissanteur']) && isset($_POST['croissante']) && isset($_POST['date']) && isset($_POST['time'])) {
            $this->load->model('croissantage_model');
                
            $date = explode('-', $_POST['date']);
            $time_regex = "/^([0-1][0-9]|2[0-3]):[0-5][0-9]$/"; // regex HH:ii

            if (checkdate($date[1], $date[2], $date[0]) && preg_match($time_regex, $_POST['time']) == 1 && $_POST['croissanteur'] != $_POST['croissante']) {
                
                $datetime = $_POST['date'] . ' ' . $_POST['time'] . ':00';
                $this->croissantage_model->create($_POST['croissanteur'], $_POST['croissante'], $datetime);
            
            } else {
                redirect('croissantage?param=addcroissantage_error');
            }
        }

        redirect('croissantage');
    }


    public function chooseViennoiserie() {

        if (isConnected() && isset($_POST['id_cr']) && isset($_POST['viennoiserie'])) {
            $this->load->model('croissantage_viennoiserie_model');

            $data = $this->croissantage_viennoiserie_model->read($_POST['id_cr'], $_SESSION['userinfo']['id']);

            if (empty($data)) {
                $this->croissantage_viennoiserie_model->create($_POST['id_cr'], $_SESSION['userinfo']['id'], $_POST['viennoiserie']);
            
            } else {
                $this->croissantage_viennoiserie_model->updateViennoiserie($_POST['id_cr'], $_SESSION['userinfo']['id'], $_POST['viennoiserie']);
            }
        }

        redirect('croissantage');
    }


    public function voirDetails() {

        if (isConnected() && isset($_POST['id_cr'])) {
            $this->load->model('croissantage_viennoiserie_model');
            $this->load->model('croissantage_model');
            $this->load->model('etudiant_model');

            $tmp_cr_infos = $this->croissantage_model->read($_POST['id_cr']);

            $data = array(
                "commandes"     =>  $this->croissantage_viennoiserie_model->readFromIdCr($_POST['id_cr']),
                "croissanteur"  =>  $this->etudiant_model->read($tmp_cr_infos['idCer']),
                "croissantage"  =>  $tmp_cr_infos
            );

            $this->load->view('voir_croissantage', $data);
        }
    }


    public function fixerDate() {

        if (isConnected() && isset($_POST['id_cr']) && isset($_POST['date']) && isset($_POST['time'])) {
            $this->load->model('croissantage_model');

            $datetime = $_POST['date'] . ' ' . $_POST['time']. ':00';

            $this->croissantage_model->updateState($_POST['id_cr'], 2);
            $this->croissantage_model->updateDeadline($_POST['id_cr'], $datetime);
        }

        redirect('profil');
    }


    public function marquerHonore() {
        
        if (isConnected() && isset($_POST['id_cr'])) {
            $this->load->model('croissantage_model');
            
            $this->croissantage_model->updateState($_POST['id_cr'], 3);
        }

        redirect('profil');
    }
}