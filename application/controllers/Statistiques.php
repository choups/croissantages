<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistiques extends CI_Controller {
    
    public function index() {
        
        if (isConnected()) {
            $this->load->model('statistiques_model');

            $victoires = $this->statistiques_model->readFromIdCer($_SESSION['userinfo']['id']);
            $defaites  = $this->statistiques_model->readFromIdCed($_SESSION['userinfo']['id']);

            $currentMonth = date("m", strtotime("now"));

            $data = array(
                "victoires"     =>  $victoires,
                "defaites"      =>  $defaites,
                "ratio"         =>  ($defaites != 0) ? round($victoires/$defaites, 2) : 0,
                "croissantages" =>  array()
            );

            for ($i = 0; $i < 6; $i++) {
                $month = date("m", strtotime("-$i month"));
                $monthLetters = date("M", strtotime("-$i month"));
                $data['croissantages']["$month/20"] = $this->statistiques_model->getCroissantagesFromMonth($monthLetters);
            }

            $this->load->view('statistiques.php', $data);

        } else {
            redirect('connexion');
        }
    }
}