<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * J'ai créé ce helper pour accéder à des méthodes comme isAdmin() depuis n'importe où dans le code
 */



if ( ! function_exists('isConnected')) {

    function isConnected() {
        return isset($_SESSION['userinfo']);
    }
    
}



if ( ! function_exists('isAdmin')) {

    function isAdmin() {
        return isConnected() && isset($_SESSION['userinfo']['droits']) && in_array("admin", $_SESSION['userinfo']['droits']);
    }
    
}