<?php
session_start();

if (isset($_GET['id_etu']) && !empty($_GET['id_etu'])) {

    if (isset($_SESSION['previousEtu'])) {
        echo 'ancien etudiant : ' . $_SESSION['previousEtu']['alias'] . "<br><br>";
    }

    $dbh = new PDO("mysql:host=localhost;dbname=croissant", "root", "root");

    $query = $dbh->prepare("SELECT * FROM etudiant WHERE id = :id_etu");
    
    $data = array(
        "id_etu" => $_GET['id_etu']
    );
    
    $query->execute($data);
    $result = $query->fetch(PDO::FETCH_ASSOC);

    echo 'etudiant actuel : ' . $result['alias'];

    $_SESSION['previousEtu'] = $result;
} 