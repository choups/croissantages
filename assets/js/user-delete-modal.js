
document.querySelectorAll('#userlist .btn').forEach((button) => {

    button.onclick = (event) => {
        let infos = getInfos(event.currentTarget);

        document.querySelector('#removeUser.modal #userlogin').innerHTML = infos.login;
        document.querySelector('#removeUser.modal input').value = infos.id;
    }
});


function getInfos(element) {
    let row = element.parentElement.parentElement;
    
    return {
        id: row.querySelector('.id').innerHTML,
        login: row.querySelector('.login').innerHTML,
        alias: row.querySelector('.alias').innerHTML
    }
}