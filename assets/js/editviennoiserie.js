
document.querySelectorAll('#viennoiserieList .btn-outline-light').forEach(button => {
    
    button.onclick = (event) => {
        let infos = getInfos(event.currentTarget);

        console.log(infos);

        document.querySelector('#editViennoiserie.modal #id_v').value = infos.id;
        document.querySelector('#editViennoiserie.modal #name_v').value = infos.name;
        document.querySelector('#editViennoiserie.modal #checkbox').checked = (infos.availability == 1);
    }
});



function getInfos(element) {
    let row = element.parentElement.parentElement;

    return {
        id: row.querySelector('.id').innerHTML,
        name: row.querySelector('.name').innerHTML,
        availability: row.querySelector('.availability input[type=hidden]').value
    };
}