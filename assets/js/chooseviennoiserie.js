
document.querySelectorAll('#croissantagesList .btn').forEach((button) => {
    
    button.onclick = (event) => {
        let infos = getInfos(event.currentTarget);
        let catchphrase = infos.croissanteur + " a croissanté " + infos.croissante + " " + infos.date;

        document.querySelector('#chooseViennoiserie.modal #catchphrase').innerHTML = catchphrase;
        document.querySelector('#chooseViennoiserie.modal input').value = infos.id;
    }
});


function getInfos(element) {
    let row = element.parentElement.parentElement;

    return {
        id: row.querySelector('.id').innerHTML,
        croissanteur: row.querySelector('.croissanteur').innerHTML,
        croissante: row.querySelector('.croissante').innerHTML,
        date: row.querySelector('.date').innerHTML
    };
}