
let selects = document.querySelectorAll('.modal select');

check();

selects.forEach((select) => {
    select.onchange = check;
});


function check() {
    let elem1 = selects[0].selectedIndex;
    let elem2 = selects[1].selectedIndex;

    setButtonState(elem1 == elem2);
}

function setButtonState(bool) {
    let button = document.querySelector('.modal .btn.btn-success');
    button.disabled = bool;
}