-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  lun. 02 mars 2020 à 19:39
-- Version du serveur :  5.7.25
-- Version de PHP :  7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `croissant`
--
DROP DATABASE IF EXISTS `croissant`;
CREATE DATABASE `croissant`;

-- --------------------------------------------------------

--
-- Structure de la table `croissantage`
--

CREATE TABLE `croissantage` (
  `id` int(11) NOT NULL,
  `idCer` int(11) NOT NULL,
  `idCed` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` int(11) DEFAULT '0',
  `deadline` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `croissantage`
--

INSERT INTO `croissantage` (`id`, `idCer`, `idCed`, `date`, `state`, `deadline`) VALUES
(4, 6, 1, '2020-02-12 13:57:47', 3, '2020-03-11 16:00:00'),
(6, 1, 7, '2020-02-19 15:14:00', 3, '2020-03-20 17:30:00'),
(7, 9, 1, '2020-02-19 15:27:00', 3, '2020-02-19 10:15:00'),
(8, 7, 1, '2020-02-29 10:48:00', 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `croissantage_viennoiserie`
--

CREATE TABLE `croissantage_viennoiserie` (
  `id_cr` int(11) NOT NULL,
  `id_etu` int(11) NOT NULL,
  `id_v` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `croissantage_viennoiserie`
--

INSERT INTO `croissantage_viennoiserie` (`id_cr`, `id_etu`, `id_v`) VALUES
(6, 1, 2),
(7, 6, 2),
(7, 7, 3),
(8, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `droit`
--

CREATE TABLE `droit` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `power` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `droit`
--

INSERT INTO `droit` (`id`, `name`, `power`) VALUES
(1, 'admin', 100),
(2, 'delegue', 50),
(3, 'etudiant', 1);

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_v` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`id`, `login`, `alias`, `password`, `id_v`) VALUES
(0, 'anonymous', 'Utilisateur supprimé', '$2y$10$U7w/ppjuMxon3hDqjLDnjuTAcusEsB9j4OWTfBq6mKLyYaKCck3Fm', 1),
(1, 'achoupau', 'choups', '$2y$10$wkopAFTnffSxvlbgWxvGNedLH/MHbVOWw5Dl/I0Yaw9w127riWh0m', 3),
(6, 'cboulch', 'corentin', '$2y$10$G/Ayn85ScR2DZBiP6u9KhOIzWELMGS6Jm8l29ix8hzq/B3.W.itDe', 1),
(7, 'jgoubet', 'julien', '$2y$10$Liwedh.PpYbW6MuRXBiFn.B.rGpGwmX/f2HXMlLjie5tODTDjHNrq', 1),
(9, 'eleprad', 'rwanito', '$2y$10$2o.SGS.7w9ZzJAXh3gSoEueIFsSLtrz..Ij/xmgGVqQ9OSH.g6z4u', 2),
(10, 'llecoz', 'Liz0ox', '$2y$10$K5h07y/L9cJN3k51hEG6au.lB8a2uj7WmCx/TM9h4PrZbaokQrLVC', 1);

-- --------------------------------------------------------

--
-- Structure de la table `etudiant_droit`
--

CREATE TABLE `etudiant_droit` (
  `id_etu` int(11) NOT NULL,
  `id_droit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiant_droit`
--

INSERT INTO `etudiant_droit` (`id_etu`, `id_droit`) VALUES
(1, 1),
(6, 3),
(7, 3),
(8, 1),
(9, 3),
(10, 3);

-- --------------------------------------------------------

--
-- Structure de la table `viennoiserie`
--

CREATE TABLE `viennoiserie` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `isAvailable` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `viennoiserie`
--

INSERT INTO `viennoiserie` (`id`, `name`, `isAvailable`) VALUES
(1, 'croissant', 1),
(2, 'Pain aux raisins', 0),
(3, 'Pain au chocolat', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `croissantage`
--
ALTER TABLE `croissantage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `croissantage_viennoiserie`
--
ALTER TABLE `croissantage_viennoiserie`
  ADD PRIMARY KEY (`id_cr`,`id_etu`,`id_v`);

--
-- Index pour la table `droit`
--
ALTER TABLE `droit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant_droit`
--
ALTER TABLE `etudiant_droit`
  ADD PRIMARY KEY (`id_etu`,`id_droit`);

--
-- Index pour la table `viennoiserie`
--
ALTER TABLE `viennoiserie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `croissantage`
--
ALTER TABLE `croissantage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `droit`
--
ALTER TABLE `droit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `viennoiserie`
--
ALTER TABLE `viennoiserie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
