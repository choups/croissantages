Spécifications:
- Le fichier à part du projet se situe à la racine. Il se nomme `PageTest.php`.
- Ce projet ayant pour contrainte d'utiliser des frameworks légers, CodeIgniter a été utilisé pour ce projet.
    - Le query builder de CodeIgniter n'a pas été utilisé. J'ai modifié le fichier system/core/Model.php afin de manipuler un objet PDO.
    - En cas de besoin les paramètres PDO se situent dans system/core/Model.php